import express from "express";
import cors from "cors";
const app = express();
// import Upload from "../src/Upload";
app.use(express.json());
app.use(cors({ origin: true }));

//GET
app.get("/", (req, res) => {
  res.status(200).send("This is HomePlus stripe API");
});

//POST
// app.post("/upload", async (req, res) => {
//   const { image, mime } = req.body;

//   try {
//     const data = await Upload(image, mime);
//     console.log(data);
//     return res.status(200).json("Task fee has been paid");
//   } catch (error) {
//     return res.status(400).json(error);
//   }
// });

app.listen(4242, () => console.log("listening on port 4242"));
